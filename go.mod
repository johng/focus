module focus

go 1.11

require (
	github.com/gogf/gf v1.16.5-0.20210630124349-1e78734f2cfd
	github.com/gogf/swagger v1.3.0
	github.com/mojocn/base64Captcha v1.3.1
	github.com/o1egl/govatar v0.3.0
	github.com/russross/blackfriday/v2 v2.1.0
)
